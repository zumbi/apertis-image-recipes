General Information
===================

Here is the recipes collection for Apertis images creation.

To use these recipes, the debos tool needs to be installed: https://github.com/go-debos/debos

By default, this command will create a minimal Apertis image for Intel 64bit systems:
```
debos apertis-ospack.yaml && debos apertis-image-amd64.yaml
```

The target architecture can be changed with command line parameter '-t architecture:armhf'.
Architectures supported:
  * arm64
  * amd64
  * armhf

Packages set selection via '-t type:minimal':
  * minimal
  * development
  * sdk

Parameters for ospack and image versioning:
  * suite: (-t suite:17.12)
  * timestamp: (-t timestamp:`date +"%s"`)


Create the ospack for the SDK package set:
```
debos -t type:sdk -t architecture:amd64 -t suite:17.12 --scratchsize 10G --debug-shell apertis-ospack.yaml
```

Create SDK image from prepared ospack:
```
debos --scratchsize 10G -t type:sdk -t suite:17.12 apertis-image-sdk.yaml
```

Building in docker
==================

To ease testing on a wide variaty of host systems (and building in Jenkins) a
docker file is provided which sets up the build environment. To build using
that:

* build the docker image:
```
docker build -t apertis-image-builder apertis-image-builder
```

* Run the image build in docker e.g:
```
docker run --device /dev/kvm  -w /recipes \
  -u $(id -u) \
  --group-add=$(getent group kvm | cut -d : -f 3) \
  -i -v $(pwd):/recipes -t apertis-image-builder \
  debos apertis-ospack.yaml
```

The image build will be run in the build docker image using your uid with the
additional kvm group (for debian based systems) to ensure debos/fakemachine can
use hardware virtualisation. For fedora/Redhat based systems the group-add is
likely not needed.
